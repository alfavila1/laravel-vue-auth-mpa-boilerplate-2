#!/bin/bash

# Set environment variables from .env file
export $(grep -v '^#' .env | xargs)

# Change docker-compose.yaml file
CURRENT_USER=$(id -un)
CURRENT_ID=$(id -u)
sed -i "s/CURRENT_USER/$CURRENT_USER/g" docker-compose.yaml
sed -i "s/CURRENT_ID/$CURRENT_ID/g" docker-compose.yaml

# Change nginx.conf file
sed -i "s/server_name .*/server_name $APP_URL;/" dev/nginx/default.conf

# Remove DB folder, if
if [ -d dev/db ]; then
  read -rp "Remove DataBase ? [y/N]: "
  if [[ "$REPLY" == [Yy]* ]]; then
      echo 'Remove DataBase ...'
      sudo rm -rf dev/db
  else
      echo 'Skip remove'
  fi
fi

# Create DB and user
sed -i "s/myuser/$DB_USERNAME/" dev/mysql/initdb.sql
sed -i "s/password/$DB_PASSWORD/" dev/mysql/initdb.sql
sed -i "s/mydb/$DB_DATABASE/" dev/mysql/initdb.sql

# Change permissions for docker
echo "Change permissions..."
sudo chown -R "$CURRENT_USER":82 .
docker compose up -d

# Install Laravel
if [ ! -d vendor ]; then
    echo "Installing vendors..."
    docker exec "$APP_SLUG" composer update
#    docker exec "$APP_SLUG" composer create-project laravel/laravel .
#    sed -i "s/APP_NAME=.*/APP_NAME=${APP_NAME}/" .env
fi

# Generate key
docker exec "$APP_SLUG" php artisan key:generate

# Add Certificate SSL
mkdir dev/certs
mkcert -cert-file dev/certs/ssl_certificate.pem -key-file dev/certs/ssl_certificate_key.pem "$APP_URL" "*.$APP_URL" localhost 127.0.0.1 ::1

# Add url to /etc/hosts file
if [ -n "$(grep -P "$APP_URL" /etc/hosts)" ]; then
  echo "Site $APP_URL already exists in hosts"
else
  echo "Adding in hosts:"
  echo "127.0.0.1 $APP_URL" | sudo tee -a /etc/hosts
fi

echo 'Change permission with excluded dev'
sudo chown -R "$CURRENT_USER":82 $(ls -I dev)
sudo chmod -R 777 storage bootstrap

echo ""
echo "Web ready:" "http://$APP_URL" and "https://$APP_URL"
echo ""



